﻿using System;

namespace Project
{
    class Program
    {

        static void Main(string[] args)
        {
   
            Candy[] candiesList = new Data().LoadCandies();

            // Infinite loop to start program over and over again.
            while (true)
            {
                // Loop will break if a candy without stock is chosen to ask again.
                while (true)
                {
                    Board.Print();
                    int inputCandyChoice = GetSelection(candiesList.Length);

                    Candy chosenCandy = GetCandy(inputCandyChoice, candiesList);

                    // Check if candies left in machine..
                    if (chosenCandy.Stock == 0)
                    {
                        Board.Print("Vide...");
                        Console.WriteLine("");
                        Console.WriteLine("-> Appuyez sur une touche pour acheter un autre bonbon...");
                        break;
                    }

                    Board.Print($"{chosenCandy}", inputCandyChoice, 
                        chosenCandy.Price);

                    PayCandy(chosenCandy, inputCandyChoice);

                    Console.WriteLine("");
                    Console.WriteLine("-> Appuyez sur une touche pour acheter un autre bonbon...");
                    Console.ReadKey();
                }
                
                Console.ReadKey();
            }
        }

        private static int GetSelection(int max)
        {
            bool checkValidInputCandy = false;
            int inputCandyChoice = 0;
            while (!checkValidInputCandy)
            {
                Console.Write("-> ");
                checkValidInputCandy = int.TryParse(Console.ReadLine(), out inputCandyChoice);
                if (checkValidInputCandy && inputCandyChoice >= 1 && inputCandyChoice <= max) continue;
                Board.Print("CHOIX INVALIDE");
                checkValidInputCandy = false;
            }
            return inputCandyChoice;
        }

        private static Candy GetCandy(int input, Candy[] candiesList) => candiesList[input - 1];

        private static decimal GetCoin()
        {
            Console.Write($"[0] - Annuler\n" +
                          $"[1] - 5c\n" +
                          $"[2] - 10c\n" +
                          $"[3] - 25c\n" +
                          $"[4] - 1$\n" +
                          $"[5] - 2$\n");
            
            int paymentInput = 0;
            bool checkValidPaymentInput = false;
            while (!checkValidPaymentInput)
            {
                Console.Write("-> ");
                checkValidPaymentInput = int.TryParse(Console.ReadLine(), out paymentInput);
                if (!checkValidPaymentInput) continue;
                if (paymentInput < 0 || paymentInput > 5) checkValidPaymentInput = false;
            }

            // Default will always be zero because of previous check
            return paymentInput switch
            {
                1 => 0.05m,
                2 => 0.10m,
                3 => 0.25m,
                4 => 1.00m,
                5 => 2.00m,
                _ => 0.00m
            };
        }

        private static void PayCandy(Candy chosenCandy, int inputCandyChoice)
        {
            decimal totalAmountReceived = 0.00m;
            decimal amountToPay = chosenCandy.Price;
            bool cancelTransaction = false;
            // Loop until nothing left to pay.
            while (amountToPay > 0)
            {
                decimal amountReceived = GetCoin();
                if (amountReceived == 0.00m) cancelTransaction = true; // Assume transaction has ben cancelled if no amount received.
                totalAmountReceived += amountReceived;
                amountToPay -= amountReceived;

                Board.Print($"{chosenCandy}", inputCandyChoice,
                    chosenCandy.Price, totalAmountReceived);

                if (!cancelTransaction) continue;

                // If transaction has been cancelled, give money back, then display cancelled transaction
                // regardless, then break from loop.
                if (totalAmountReceived != 0)
                {
                    Board.Print("Prenez votre monnaie...", inputCandyChoice,
                        chosenCandy.Price, totalAmountReceived, totalAmountReceived);
                    Console.ReadLine();
                }

                Board.Print("Transaction annulée..", inputCandyChoice,
                    chosenCandy.Price);
                break;
            }
            // If transaction hasn't been cancelled, then give out the candy.
            // Using ternary operator, make negative number positive for to show change given back, otherwise passing zero. 
            // Zero will be display as ----- by print method.
            if (!cancelTransaction)
            {
                Board.Print("Prenez votre friandise...", inputCandyChoice, chosenCandy.Price,
                    totalAmountReceived, amountToPay < 0 ? amountToPay * -1.00m : amountToPay, $"{chosenCandy}");
                chosenCandy.Stock -= 1;
            }

        }
    }
}
